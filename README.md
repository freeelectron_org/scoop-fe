# scoop-fe

scoop bucket for FE

## Coverage

Currently, this archive is focused on running FE operators in Houdini and Maya.

Houdini Versions
- 19.5.303

Maya Versions
- 2023

## Install Scoop

Using PowerShell,

```powershell
set-executionpolicy remotesigned -scope currentuser
iwr -useb get.scoop.sh | iex
```

Scoop uses symbolic links, which requires that you activate Developer Mode
or use scoop in an administrator shell.

```
Settings -> Update & Security -> For Developers ... select “Developer mode”
```

## Add Bucket

```shell
scoop bucket add freeelectron_org https://gitlab.com/freeelectron_org/scoop-fe.git
```

## Install App

```shell
scoop install freeelectron
```

To update,

```shell
scoop update
scoop update freeelectron
```

or just update everything

```shell
scoop update --all
```

To force an update,

```shell
scoop update
scoop update -kf freeelectron
```

## Uninstall App

```shell
scoop uninstall freeelectron
```

## Run Python

The scoop install could forceibly change your environment variables,
but it is safer for you to just set up PYTHONPATH manually.

```shell
set PYTHONPATH="%PYTHONPATH%;%HOMEDRIVE%%HOMEPATH%\scoop\apps\freeelectron\current\fe\lib\x86_win64_optimize"
```

```shell
python3.9

import pyfe.context
fe = pyfe.context.Context()

help(pyfe.context)
```

## Run Houdini

In a shell, run with the scoop shim:

```shell
run_houdini
```

If you type "run_houdini" into the taskbar search box,
you should get an option to execute the shim as a command.

This simple script just appends some environment variables
(PATH and HOUDINI_PATH) before running houdini normally.
If you would rather stick with running Houdini directly,
you can adjust environment variables per the documentation.

https://www.sidefx.com/docs/houdini/basics/config_env.html

Even better, you can just grab the Houdini package FreeElectron.json
from the archive and copy it somewhere in your package path,
such as under $HOUDINI_USER_PREF_DIR/packages.

## Run Maya

In a shell, run with the scoop shim:

```shell
run_maya
```

If you type "run_maya" into the taskbar search box,
you should get an option to execute the shim as a command.

This simple script just appends some environment variables
(PATH, MAYA_PLUG_IN_PATH, MAYA_SCRIPT_PATH, and XBMLANGPATH)
before running maya normally.
